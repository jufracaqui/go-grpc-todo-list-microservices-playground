package grpc

import (
	"context"
	"net"
	"os"
	"os/signal"

	"google.golang.org/grpc"

	"github.com/jufracaqui/todo-list/pkg/api/v1"
	"github.com/jufracaqui/todo-list/pkg/logger"
	"github.com/jufracaqui/todo-list/pkg/protocol/grpc/middleware"
)

// RunServer run gRPC service to publish ToDo service
func RunServer(ctx context.Context, v1API v1.ToDoServiceServer, port string) error {
	listen, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}

	// gRPC server statup options
	opts := []grpc.ServerOption{}

	// add middleware
	opts = middleware.AddLogging(logger.Log, opts)

	// Register service
	server := grpc.NewServer(opts...)
	v1.RegisterToDoServiceServer(server, v1API)

	// Graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			// sig is a ^C, handle it
			logger.Log.Warn("Shutting down gRPC server...")

			server.GracefulStop()

			<-ctx.Done()
		}
	}()

	// Start gRPC server
	logger.Log.Info("Starting gRPC server...")
	return server.Serve(listen)
}
