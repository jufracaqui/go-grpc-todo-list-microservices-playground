# TODO LIST

## Requirements
- Go >= 1.11
- [Protobuf](https://github.com/protocolbuffers/protobuf/releases) installed and added to PATH
- [Go code generator plugin](https://github.com/golang/protobuf/protoc-gen-go) for proto compiler
- [grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway)
- [protoc-gen-swagger](https://github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger)
- [zap logging](https://github.com/uber-go/zap)